package com.example.mycalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn0,btnAdd,btnSub,btnMulti,btnDiv,btnEqual,btnDot;
    Button btnClear;
    EditText ed1;
    float Res1,Res2;
    boolean  Add,Sub,Multi,Div;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn1=(Button)findViewById(R.id.Btn1);
        btn2=(Button)findViewById(R.id.Btn2);
        btn3=(Button)findViewById(R.id.Btn3);
        btn4=(Button)findViewById(R.id.Btn4);
        btn5=(Button)findViewById(R.id.Btn5);
        btn6=(Button)findViewById(R.id.Btn6);
        btn7=(Button)findViewById(R.id.Btn7);
        btn8=(Button)findViewById(R.id.Btn8);
        btn9=(Button)findViewById(R.id.Btn9);
        btn0=(Button)findViewById(R.id.Btn0);
        btnAdd=(Button)findViewById(R.id.BtnAdd);
        btnSub=(Button)findViewById(R.id.BtnSub);
        btnMulti=(Button)findViewById(R.id.BtnMulti);
        btnDiv=(Button)findViewById(R.id.BtnDiv);
        btnEqual=(Button)findViewById(R.id.button15BtnEqual);
        btnDot=(Button)findViewById(R.id.BtnDot);
        btnClear=(Button)findViewById(R.id.BtnClear);
        ed1=(EditText)findViewById(R.id.editTextTextPersonName);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1.setText(ed1.getText() + "1");
            }
        }) ;
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1.setText(ed1.getText() + "2");
            }
        }) ;
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1.setText(ed1.getText() + "3");
            }
        }) ;
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1.setText(ed1.getText() + "4");
            }
        }) ;
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1.setText(ed1.getText() + "5");
            }
        }) ;
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1.setText(ed1.getText() + "6");
            }
        }) ;
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1.setText(ed1.getText() + "7");
            }
        }) ;
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1.setText(ed1.getText() + "8");
            }
        }) ;
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1.setText(ed1.getText() + "9");
            }
        }) ;
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1.setText(ed1.getText() + "0");
            }
        }) ;
        btnDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1.setText(ed1.getText() + ".");
            }
        }) ;
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed1 == null) {
                    ed1.setText("");
                }
                else {
                    Res1 = Float.parseFloat(ed1.getText() + "");
                    Add=true;
                    ed1.setText(null);
                }
            }
        });
        btnSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed1 == null) {
                    ed1.setText("");
                }
                else {
                    Res1 = Float.parseFloat(ed1.getText() + "");
                    Sub=true;
                    ed1.setText(null);
                }
            }
        });
        btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed1 == null) {
                    ed1.setText("");
                }
                else {
                    Res1 = Float.parseFloat( ed1.getText() + "");
                    Multi=true;
                    ed1.setText(null);
                }
            }
        });
        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed1 == null) {
                    ed1.setText("");
                }
                else {
                    Res1 = Float.parseFloat( ed1.getText() + "");
                    Div=true;
                    ed1.setText(null);
                }
            }
        });
        btnEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Res2=Float.parseFloat(ed1.getText()+"");
                if (Add==true) {
                    ed1.setText(Res1 + Res2 + "");
                    Add = false;
                }
                if (Sub==true) {
                    ed1.setText(Res1 - Res2 + "");
                    Sub = false;
                }
                if (Multi==true) {
                    ed1.setText(Res1 * Res2 + "");
                    Multi = false;
                }
                if (Div==true) {
                    ed1.setText(Res1 / Res2 + "");
                    Div = false;
                }
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1.setText(" ");

            }
        });
    }
}